# Changelog

## Unreleased
- Update crates and fix errors with Nightly code

## 0.0.1 (2021-03-25)
- Update code to build on 2021 nightly

## 0.0.0 (2019-08-01)
- Start TANTIVEOS project
- Create minimal 64 bit Rust kernel for x86 arch
- Print to the screen using VGA text mode
- Add basic unit and integration testing