# Shell
## Builtin Commands
**Help** command:
    > help

**Uptime** command:
	> uptime

**VERSION** command:
	> version
Prints out uptime in seconds
## TODO:
- Config file that the shell can read during init to set stuff like the prompt, colors, etc
- Add some builtins to the shell
  - Uptime
  - Version
  - Help
- Add shell history 
- Add a cursor
- Add some cli args to uptime ex display milliseconds instead of seconds
