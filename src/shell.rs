use crate::{clock, print, println};
use alloc::string::String;
use lazy_static::lazy_static;
use spin::Mutex;

lazy_static! {
    pub static ref STDIN: Mutex<String> = Mutex::new(String::new());
}

pub fn key_handle(c: char) {
    let mut stdin = STDIN.lock();
    if c == '\n' {
        print!("\n");
        match stdin.as_str() {
            "help" => {
                print!("I would also like some help");
            }
            "version" => {
                print!("TANTIVE OS v{}", env!("CARGO_PKG_VERSION"));
            }
            "uptime" => {
                print!("{:.6} seconds", clock::uptime());
            }
            _ => {
                print!("Unknown Command");
            }
        }
        stdin.clear();
        print_prompt()
    } else {
        // skip if its a comment
        if c == 0x08 as char {
            if stdin.len() > 0 {
                stdin.pop();
                print!("{}", c);
            }
        } else {
            stdin.push(c);
            print!("{}", c);
        }
    }
}

pub fn init_shell() {
    todo!()
}

pub fn print_tantive_specs() {
    println!("Welcome to Tantive OS.");
    println!("Tantive IV");
    println!("Length: {} Meters", 126);
    println!("Propulsion: Class 2-CEC Subspace Hyperdrive");
    println!("Class: CR90 corvette");
    println!("Affiliation: Royal House of Alderaan");
    println!("Launched: 22 BBY");
    println!("Decomissioned: 36 ABY");
    println!("Captain: Raymus Antilles");
    println!("Registry: Corellian Engineering Corporation");
}

pub fn print_prompt() {
    print!("\n> ");
}
