#![no_std]
#![no_main]
#![feature(custom_test_frameworks)]
#![test_runner(tantive_os::test_runner)]
#![reexport_test_harness_main = "test_main"]

extern crate alloc;

use alloc::vec;
use bootloader::{entry_point, BootInfo};
use core::panic::PanicInfo;
use tantive_os::gdt::print_gdt;
use tantive_os::memory::BootInfoFrameAllocator;
use tantive_os::shell;
use tantive_os::task::{executor::Executor, keyboard, Task};
use tantive_os::{allocator, memory, println};
use x86_64::VirtAddr;
mod io;

entry_point!(kernel_main);

fn kernel_main(boot_info: &'static BootInfo) -> ! {
    tantive_os::init();
    shell::print_tantive_specs();
    shell::print_prompt();

    //invoke a breakpoint exception
    // x86_64::instructions::interrupts::int3();

    // trigger a page fault, should be stuck in an endless loop
    // let ptr = 0xdeadbeaf as *mut u8;
    // unsafe { *ptr = 42; }

    // let phys_mem_offset = VirtAddr::new(boot_info.physical_memory_offset);
    // let mut mapper = unsafe { memory::init(phys_mem_offset) };
    // let mut frame_allocator = unsafe { BootInfoFrameAllocator::init(&boot_info.memory_map) };
    // // map an unused page
    // let page = Page::containing_address(VirtAddr::new(0xdeadbeaf000));
    // memory::create_example_mapping(page, &mut mapper, &mut frame_allocator);
    // // write the string `New!` to the screen through the new mapping
    // let page_ptr: *mut u64 = page.start_address().as_mut_ptr();
    // unsafe { page_ptr.offset(400).write_volatile(0x_f021_f077_f065_f04e) };

    let phys_mem_offset = VirtAddr::new(boot_info.physical_memory_offset);
    let mut mapper = unsafe { memory::init(phys_mem_offset) };
    let mut frame_allocator = unsafe { BootInfoFrameAllocator::init(&boot_info.memory_map) };

    allocator::init_heap(&mut mapper, &mut frame_allocator).expect("heap initialization failed");
    let mut port_manager = io::port_manager::PortManager::new();

    println!("A vector: {:?}", vec![1, 2, 3, 4, 5]);
    let a_map: hashbrown::HashMap<&'static str, i32> =
        [("test", 1), ("test2", 2)].into_iter().collect();
    println!("A map: {:?}", a_map);
    // rtc test code
    let mut rtc = io::rtc::Rtc::new(&mut port_manager).expect("Failed to construct rtc");
    let mut date = rtc.read();
    println!("Current datetime UTC: {}", date);
    date.convert_to_chicago_time();
    println!("Current datetime Chicago: {}", date);
    // date.hours -= 1;
    // rtc.write(&date);
    // let date = rtc.read();
    // println!("Current date modified in cmos: {:?}", date);

    unsafe {
        print_gdt();
    }
    #[cfg(test)]
    test_main();
    // panic!("Many Bothans died to bring you this panic message");

    let mut executor = Executor::new();
    executor.spawn(Task::new(keyboard::print_keypresses()));
    executor.run();
}

/// This function is called on panic.
#[cfg(not(test))]
#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    println!("{}", info);
    tantive_os::hlt_loop();
}

#[cfg(test)]
#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    tantive_os::test_panic_handler(info)
}

#[test_case]
fn trivial_assertion() {
    assert_eq!(1, 1);
}
