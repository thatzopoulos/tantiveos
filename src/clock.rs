use lazy_static::lazy_static;
use spin::Mutex;

lazy_static! {
    pub static ref TICKS: Mutex<usize> = Mutex::new(0);
}

pub fn tick() {
    let mut ticks = TICKS.lock();
    *ticks += 1;
}

pub fn uptime() -> f64 {
    let ticks = *TICKS.lock();
    // Magic numbers below are chosen after reading up on the PIT:
    // https://en.wikibooks.org/wiki/X86_Assembly/Programmable_Interval_Timer
    // https://wiki.osdev.org/Programmable_Interval_Timer#The_Oscillator
    // 1193182 hz is the signal emitted by the Programmable Interval Timer
    // We need to divide that signal by frequency f, which is 16 bits with a value of 0 being interpreted as 65536
    // This gives us a final value for the resolution of the frequency of 1193182 / 65536 ≈ 18.2065 hz
    1.0 / (1.193182 * 1000000.0 / 65536.0) * ticks as f64
}
