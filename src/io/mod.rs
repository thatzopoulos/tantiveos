use core::cell::RefCell;
use port_manager::{Port, PortManager};

pub mod port_manager;
pub mod rtc;

struct ExitPort {
    port: RefCell<Option<Port>>,
}

impl ExitPort {
    const fn new() -> ExitPort {
        ExitPort {
            port: RefCell::new(None),
        }
    }
}
// RefCell is not sync, but we only have one thread...
unsafe impl Sync for ExitPort {}

static EXIT_PORT: ExitPort = ExitPort::new();

pub fn init_late(port_manager: &mut PortManager) {
    const ISA_DEBUG_EXIT_PORT_NUM: u16 = 0xf4;
    let mut port = EXIT_PORT.port.borrow_mut();
    *port = Some(
        port_manager
            .request_port(ISA_DEBUG_EXIT_PORT_NUM)
            .expect("Failed to get exit port"),
    )
}

pub unsafe fn exit(code: u8) {
    let mut port = EXIT_PORT.port.borrow_mut();
    port.as_mut()
        .expect("exit port not initialized")
        .writeb(code);
}
