/// MY RTC implementation is based on StreamOS's early RTC Code: https://github.com/sphaerophoria/stream-os/commit/5ec2898ec070f68197a97723a9ac30ae03d3e074#diff-72ef898a77aa664fcc63de579c878c87e101688fc902cb1922fc884d7b2bea65
use core::fmt;

use crate::io::{Port, PortManager};
use thiserror_no_std::Error;

const NMI_ENABLE: bool = true;

#[derive(Debug)]
pub struct DateTime {
    pub seconds: u8,
    pub minutes: u8,
    pub hours: u8,
    pub weekday: u8,
    pub day: u8,
    pub month: u8,
    pub year: u8,
    pub century: u8,
}

impl DateTime {
    fn weekday_name(&self) -> &'static str {
        match self.weekday {
            0 => "Sunday",
            1 => "Monday",
            2 => "Tuesday",
            3 => "Wednesday",
            4 => "Thursday",
            5 => "Friday",
            6 => "Saturday",
            _ => "Unknown",
        }
    }
}
impl fmt::Display for DateTime {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{:02}/{:02}/{:02}{:02} {:02}:{:02}:{:02} {}",
            self.month,
            self.day,
            self.century,
            self.year,
            self.hours,
            self.minutes,
            self.seconds,
            self.weekday_name()
        )
    }
}

fn get_nmi_mask(nmi_enable: bool) -> u8 {
    if nmi_enable {
        0
    } else {
        1 << 7
    }
}

fn select_reg(control_port: &mut Port, nmi_enable: bool, reg: u8) {
    control_port.writeb(get_nmi_mask(nmi_enable) | reg);
}

fn read_cmos_reg(control_port: &mut Port, data_port: &mut Port, nmi_enable: bool, reg: u8) -> u8 {
    select_reg(control_port, nmi_enable, reg);
    data_port.readb()
}

fn write_cmos_reg(
    control_port: &mut Port,
    data_port: &mut Port,
    nmi_enable: bool,
    reg: u8,
    val: u8,
) {
    select_reg(control_port, nmi_enable, reg);
    data_port.writeb(val);
}

fn update_in_progress(control_port: &mut Port, data_port: &mut Port, nmi_enable: bool) -> bool {
    const STATUS_REG_A_NUM: u8 = 0x0a;
    select_reg(control_port, nmi_enable, STATUS_REG_A_NUM);
    in_progress_set(data_port.readb())
}

fn in_progress_set(status_reg_a: u8) -> bool {
    const IN_PROGRESS_MASK: u8 = 1 << 7;
    status_reg_a & IN_PROGRESS_MASK == IN_PROGRESS_MASK
}

fn set_data_format(cmos_nmi_control_port: &mut Port, cmos_data_port: &mut Port, nmi_enable: bool) {
    const STATUS_REG_B_NUM: u8 = 0x0b;
    let mut status_reg = read_cmos_reg(
        cmos_nmi_control_port,
        cmos_data_port,
        nmi_enable,
        STATUS_REG_B_NUM,
    );
    status_reg |= 1 << 1; // Enables 24 hour mode
    status_reg |= 1 << 2; // Enables binary format of retrieved values

    write_cmos_reg(
        cmos_nmi_control_port,
        cmos_data_port,
        nmi_enable,
        STATUS_REG_B_NUM,
        status_reg,
    );
}

fn update_guarded_op<R, F: Fn(&mut Port, &mut Port) -> R>(
    control_port: &mut Port,
    data_port: &mut Port,
    f: F,
) -> R {
    let mut ret;
    loop {
        while update_in_progress(control_port, data_port, NMI_ENABLE) {
            continue;
        }

        ret = f(control_port, data_port);

        if update_in_progress(control_port, data_port, NMI_ENABLE) {
            continue;
        }

        break;
    }

    ret
}
#[derive(Debug, Error)]
pub enum RtcInitError {
    #[error("failed to get control port")]
    FailedToGetControlPort,
    #[error("failed to get data port")]
    FailedToGetDataPort,
}

pub struct Rtc {
    cmos_nmi_control_port: Port,
    cmos_data_port: Port,
}

impl Rtc {
    pub fn new(port_manager: &mut PortManager) -> Result<Rtc, RtcInitError> {
        use RtcInitError::*;
        let mut cmos_nmi_control_port = port_manager
            .request_port(0x70)
            .ok_or(FailedToGetControlPort)?;
        let mut cmos_data_port = port_manager.request_port(0x71).ok_or(FailedToGetDataPort)?;

        set_data_format(&mut cmos_nmi_control_port, &mut cmos_data_port, NMI_ENABLE);

        Ok(Rtc {
            cmos_nmi_control_port,
            cmos_data_port,
        })
    }

    // allows you to manually set the date and time in the RTC Hardware in case you need to set the initial system time / manually adjust the time / sync the hardware clock with a network time source / etc.
    pub fn write(&mut self, date_time: &DateTime) {
        update_guarded_op(
            &mut self.cmos_nmi_control_port,
            &mut self.cmos_data_port,
            |control_port, data_port| {
                write_cmos_reg(control_port, data_port, NMI_ENABLE, 0x00, date_time.seconds);
                write_cmos_reg(control_port, data_port, NMI_ENABLE, 0x02, date_time.minutes);
                write_cmos_reg(control_port, data_port, NMI_ENABLE, 0x04, date_time.hours);
                write_cmos_reg(control_port, data_port, NMI_ENABLE, 0x06, date_time.weekday);
                write_cmos_reg(control_port, data_port, NMI_ENABLE, 0x07, date_time.day);
                write_cmos_reg(control_port, data_port, NMI_ENABLE, 0x08, date_time.month);
                write_cmos_reg(control_port, data_port, NMI_ENABLE, 0x09, date_time.year);
                write_cmos_reg(control_port, data_port, NMI_ENABLE, 0x32, date_time.century);
            },
        );
    }

    pub fn read(&mut self) -> DateTime {
        update_guarded_op(
            &mut self.cmos_nmi_control_port,
            &mut self.cmos_data_port,
            |control_port, data_port| {
                let seconds = read_cmos_reg(control_port, data_port, NMI_ENABLE, 0x00);
                let minutes = read_cmos_reg(control_port, data_port, NMI_ENABLE, 0x02);
                let hours = read_cmos_reg(control_port, data_port, NMI_ENABLE, 0x04);
                let weekday = read_cmos_reg(control_port, data_port, NMI_ENABLE, 0x06)
                    .checked_sub(1)
                    .unwrap_or(6); // RTC should never return 0, just unwrap is probably fine
                let day = read_cmos_reg(control_port, data_port, NMI_ENABLE, 0x07);
                let month = read_cmos_reg(control_port, data_port, NMI_ENABLE, 0x08);
                let year = read_cmos_reg(control_port, data_port, NMI_ENABLE, 0x09);
                let century = read_cmos_reg(control_port, data_port, NMI_ENABLE, 0x32);

                DateTime {
                    seconds,
                    minutes,
                    hours,
                    weekday,
                    day,
                    month,
                    year,
                    century,
                }
            },
        )
    }
}

impl DateTime {
    pub fn convert_to_chicago_time(&mut self) {
        // First determine if we're in DST
        let is_dst = self.is_dst();
        let offset = if is_dst { 5 } else { 6 };

        // Store original values before modification
        let orig_hours = self.hours;
        let orig_day = self.day;
        let orig_month = self.month;
        let orig_year = self.year;
        // let orig_century = self.century;
        let orig_weekday = self.weekday;

        // Handle hour conversion
        if orig_hours < offset {
            self.hours = orig_hours + 24 - offset;

            // We need to go back one day
            if orig_day == 1 {
                if orig_month == 1 {
                    // Going back to previous year
                    self.month = 12;
                    if orig_year == 0 {
                        self.year = 99;
                        self.century -= 1;
                    } else {
                        self.year = orig_year - 1;
                    }
                    self.day = 31; // December always has 31 days
                } else {
                    // Going back to previous month
                    self.month = orig_month - 1;
                    self.day = self.days_in_month();
                }
            } else {
                self.day = orig_day - 1;
            }

            // Adjust weekday (going backwards)
            self.weekday = if orig_weekday == 0 {
                6
            } else {
                orig_weekday - 1
            };
        } else {
            self.hours = orig_hours - offset;
        }
    }

    fn is_dst(&self) -> bool {
        let dst_start = self.calculate_nth_weekday_of_month(2, 0, 3); // 2nd Sunday of March
        let dst_end = self.calculate_nth_weekday_of_month(1, 0, 11); // 1st Sunday of November

        let current_date = self.month as u16 * 100 + self.day as u16;
        let start_date = 300 + dst_start as u16;
        let end_date = 1100 + dst_end as u16;

        current_date >= start_date && current_date < end_date
    }

    fn calculate_nth_weekday_of_month(&self, nth: u8, weekday: u8, month: u8) -> u8 {
        let year = (self.century as u16 * 100 + self.year as u16) as i32;
        let q = month as i32;
        let m = if q < 3 { q + 12 } else { q };
        let j = year / 100;
        let k = year % 100;
        let f = 1 + k + k / 4 + j / 4 + 5 * j;
        let first_day_of_month = (f + (m + 1) * 26 / 10) % 7;

        let first_weekday =
            (first_day_of_month + (weekday as i32 - first_day_of_month + 7) % 7) % 7;

        // Calculate the nth weekday of the month
        let result_day = (nth as i32 - 1) * 7 + first_weekday + 1; // +1 because days start at 1
        if result_day > 7 {
            // Ensure the result fits within the typical month bounds
            result_day as u8
        } else {
            7 // Fallback in case calculation underflows (less likely but safer to handle)
        }
    }

    fn days_in_month(&self) -> u8 {
        match self.month {
            4 | 6 | 9 | 11 => 30,
            2 => {
                if self.is_leap_year() {
                    29
                } else {
                    28
                }
            }
            _ => 31,
        }
    }

    fn is_leap_year(&self) -> bool {
        let year = (self.century as u16 * 100 + self.year as u16) as i32;
        (year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)
    }
}
