use crate::println;
use core::arch::asm;
use core::mem::{self, MaybeUninit};
use core::{fmt, ptr};
/// https://web.archive.org/web/20190217233448/https://www.flingos.co.uk/docs/reference/Global-Descriptor-Table/
/// Global Descriptor table was used for memory segmentation before paging became the standard. One of the things we still need it for
/// is TSS loading. Later we will need it for switching between kernel space and user space
use lazy_static::lazy_static;
use x86_64::structures::gdt::{Descriptor, GlobalDescriptorTable, SegmentSelector};
use x86_64::structures::tss::TaskStateSegment;
use x86_64::VirtAddr;

const STACK_SIZE: usize = 1024 * 8; // Define a standard stack size
pub const DOUBLE_FAULT_IST_INDEX: u16 = 0; // Index for the double fault stack
pub const PAGE_FAULT_IST_INDEX: u16 = 1; // Index for the page fault stack
pub const GENERAL_PROTECTION_FAULT_IST_INDEX: u16 = 2; // Index for the general protection fault stack

lazy_static! {
    static ref TSS: TaskStateSegment = {
        let mut tss = TaskStateSegment::new();

        // Setting up the ESP0 stack
        static mut ESP0_STACK: [u8; STACK_SIZE] = [0; STACK_SIZE];
        let esp0_stack_end = VirtAddr::from_ptr(unsafe { &ESP0_STACK }) + STACK_SIZE;
        tss.privilege_stack_table[0] = esp0_stack_end;

        // Setting up the double fault stack
        static mut DOUBLE_FAULT_STACK: [u8; STACK_SIZE] = [0; STACK_SIZE];
        let double_fault_stack_end = VirtAddr::from_ptr(unsafe { &DOUBLE_FAULT_STACK }) + STACK_SIZE;
        tss.interrupt_stack_table[DOUBLE_FAULT_IST_INDEX as usize] = double_fault_stack_end;

        // Setting up the page fault stack
        static mut PAGE_FAULT_STACK: [u8; STACK_SIZE] = [0; STACK_SIZE];
        let page_fault_stack_end = VirtAddr::from_ptr(unsafe { &PAGE_FAULT_STACK }) + STACK_SIZE;
        tss.interrupt_stack_table[PAGE_FAULT_IST_INDEX as usize] = page_fault_stack_end;

        // Setting up the general protection fault stack // TODO : Needs an entry in the IDT? This is mainly useful for segmentation but I use paging so I might not even need it
        static mut GENERAL_PROTECTION_FAULT_STACK: [u8; STACK_SIZE] = [0; STACK_SIZE];
        let gpf_stack_end = VirtAddr::from_ptr(unsafe { &GENERAL_PROTECTION_FAULT_STACK }) + STACK_SIZE;
        tss.interrupt_stack_table[GENERAL_PROTECTION_FAULT_IST_INDEX as usize] = gpf_stack_end;

        tss
    };
}

lazy_static! {
    static ref GDT: (GlobalDescriptorTable, Selectors) = {
        let mut gdt = GlobalDescriptorTable::new();
    // Kernel code segment is used to define a memory area where the executable code of the kernel resides. In x86 protected mode, the code segment descriptor will have the "executable" bit set, and often the "readable" bit set too (allowing the CPU to read data from the code segment), but not the "writable" bit (preventing data from being written to the code segment).
        let code_selector = gdt.add_entry(Descriptor::kernel_code_segment());
        let tss_selector = gdt.add_entry(Descriptor::tss_segment(&TSS));
        let user_code = gdt.add_entry(Descriptor::user_code_segment());
        let user_data = gdt.add_entry(Descriptor::user_data_segment());
        (
            gdt,
            Selectors {
                code_selector,
                tss_selector,
                user_code,
                user_data,
            },
        )
    };
}

struct Selectors {
    code_selector: SegmentSelector,
    tss_selector: SegmentSelector,
    pub user_code: SegmentSelector,
    pub user_data: SegmentSelector,
}

pub fn init() {
    use x86_64::instructions::segmentation::{Segment, CS};
    use x86_64::instructions::tables::load_tss;

    GDT.0.load();
    unsafe {
        CS::set_reg(GDT.1.code_selector);
        load_tss(GDT.1.tss_selector);
    }
}

// custom stuff to try and print out the GDT
#[repr(C, packed)]
struct GdtDescriptor {
    limit_low: u16,
    base_low: u16,
    base_mid: u8,
    access: u8,
    limit_high: u8,
    base_high: u8,
}

impl GdtDescriptor {
    fn base(&self) -> u32 {
        let base = (self.base_low as u32)
            | ((self.base_mid as u32) << 16)
            | ((self.base_high as u32) << 24);
        u32::from_le(base)
    }

    fn limit(&self) -> u32 {
        let limit = (self.limit_low as u32) | (((self.limit_high & 0xF) as u32) << 16);
        u32::from_le(limit)
    }
}

#[repr(C, packed)]
struct Gdt {
    limit: u16,
    base: u64,
}

pub fn read_gdtr() -> Gdt {
    let mut gdtr = MaybeUninit::<Gdt>::uninit();
    unsafe {
        asm!(
            "sgdt [{}]",
            in(reg) gdtr.as_mut_ptr(),
            options(nostack, preserves_flags)
        );
        gdtr.assume_init()
    }
}

pub unsafe fn print_gdt() {
    let gdtr = read_gdtr();
    let base_ptr = ptr::addr_of!(gdtr.base);
    let limit_ptr = ptr::addr_of!(gdtr.limit);

    // Use ptr::read_unaligned to safely read values from the packed struct
    let gdt_base = ptr::read_unaligned(base_ptr);
    let gdt_limit = ptr::read_unaligned(limit_ptr) as usize;

    println!("GDT base: {:#x}, limit: {:#x}", gdt_base, gdt_limit);

    let base = gdt_base as *const GdtDescriptor;
    for i in 0..(gdt_limit / mem::size_of::<GdtDescriptor>()) {
        let segment_ptr = base.add(i);
        let segment = ptr::read_unaligned(segment_ptr);
        println!("Segment {i}:");
        println!(
            "base: {:#x}, limit: {:#x}, access: {:#x}, flags: {:#x}",
            segment.base(),
            segment.limit(),
            segment.access,
            segment.limit_high >> 4
        );
    }
}
