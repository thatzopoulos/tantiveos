# Tantive OS: A Rust Kernel

![BootScreen](boot.png?raw=true "BootScreen")

Tantive OS is a 64 bit kernel written in Rust in the very early stages of development. It currently can print to the screen using VGA Text mode, can run rudimentary integration tests, and has partially implemented CPU Exceptions. The most recent additions are a paging implementation and basic heap allocation.

Prerequisites:

To build TantiveOS, you need to have the nightly version of rust with a version past 07-25-2020, and the following:

```
rustup component add rust-src
rustup component add llvm-tools-preview
cargo install bootimage
```

You will also need qemu installed:
```
brew install qemu
# or 
pacman -S qemu
```

Run:

To build a disk image and launch it using qemu, run:

```cargo run --release```


Tests:

To run tests, run:

```cargo test```

Tests are run using a custom test framework that allows me to execute test functions inside of the kernel. The results of the tests are grabbed from qemu using qemu and bootimage.